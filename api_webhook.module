<?php
use Drupal\api_webhook\Hook;

/**
 * Implements hook_menu().
 */
function api_webhook_menu() {
  $items['api-webhook/%/%'] = array(
    'title'            => 'API webhook',
    'access arguments' => array('access content'),
    'page callback'    => 'api_webhook_page_callback',
    'page arguments'   => array(1, 2),
  );
  return $items;
}

/**
 * Page callback for /api-webhook/%/%
 * @param string $project_name
 * @param string $branch_name
 * @return string
 */
function api_webhook_page_callback($project_name, $branch_name) {
  if (!$branch = api_get_branch_by_name($project_name, $branch_name)) {
    return drupal_not_found();
  }

  if (is_string($branch->data)) {
    $branch->data = unserialize($branch->data);
  }

  // Flag the branch to be processed in next cron.
  $branch->data['api_webhook_process'] = TRUE;
  api_save_branch($branch);

  return 'Queued to process on next cron run.';
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function api_webhook_form_api_branch_edit_form_alter(&$form, &$form_state) {
  $branch = $form['#branch'];

  $form['data']['download_url'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Download URL'),
    '#default_value' => isset($branch->download_url) ? $branch->download_url : '',
    '#description'   => t('Paths to download the source code, one per line. Use "-" to ignore'),
    '#weight'        => -10,
  );
}

/**
 * Implements hook_cron().
 */
function api_webhook_cron() {
  foreach (api_get_branches() as $branch) {
    if (!empty($branch->download_url) && isset($branch->api_webhook_process)) {
      // There's an other process on same branch.
      is_string($branch->data) && $branch->data = unserialize($branch->data);
      if (FALSE !== $branch->data['api_webhook_process']) {
        api_webhook_cron_branch($branch);
      }
    }
  }
}

/**
 * Wrapper function to process a branch.
 * @param stdClass $branch
 */
function api_webhook_cron_branch($branch) {
  // Say we are processing the branch
  #is_string($branch->data) && $branch->data = unserialize($branch->data);
  #$branch->data['api_webhook_process'] = FALSE;
  #api_save_branch($branch);

  // Actually process
  $hook = new Hook($branch);
  $hook->execute();

  // Processed, remove the flag
  #is_string($branch->data) && $branch->data = unserialize($branch->data);
  #unset($branch->data['api_webhook_process']);
  #api_save_branch($branch);
}
